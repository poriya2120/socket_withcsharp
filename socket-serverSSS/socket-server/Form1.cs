﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace socket_server
{   
    
    public partial class Form1 : Form
    {

        Socket SocServer = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        Socket SocClient = null;
        public Form1()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        //    string s = "192.168.239.1";
        //    string d = "5050";
        //textBox1.Text = d;
        //    textBox2.Text = s;
        }
        public void GetMsg()
        {
            try
            {
                while (true)
                {
                    byte[] barray = new byte[1024];
                    int RecB = SocClient.Receive(barray);
                    if (RecB > 0)
                    {
                        ListShowmsg.Items.Add("Cilent: " + Encoding.Unicode.GetString(barray, 0, RecB));
                    }
                }
            }
            catch
            {
                ;
            }
        }
        public void StartServer()
        {
            IPEndPoint ipendpointserver = new IPEndPoint(IPAddress.Any, int.Parse(txtPort.Text));
            SocServer.Bind(ipendpointserver);
            SocServer.Listen(1);
            MessageBox.Show("Start server");
            SocClient = SocServer.Accept();
            Thread trGetMsg = new Thread(new ThreadStart(GetMsg));
            trGetMsg.Start();
        }







        private void button1_Click(object sender, EventArgs e)
        {

            Thread TrStart = new Thread(new ThreadStart(StartServer));
            TrStart.Start();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (SocClient != null)
                {
                    SocClient.Shutdown(SocketShutdown.Both);

                }
                if (SocServer != null)
                {
                    SocServer.Shutdown(SocketShutdown.Both);
                }
                Environment.Exit(Environment.ExitCode);
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Environment.Exit(Environment.ExitCode);
                Application.Exit();

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (SocClient != null)
                {
                    SocClient.Shutdown(SocketShutdown.Both);

                }
                if (SocServer != null)
                {
                    SocServer.Shutdown(SocketShutdown.Both);
                }
                Environment.Exit(Environment.ExitCode);
                Application.Exit();
            }
            catch {
                ;

            }


           

           
            
        }



    private void button4_Click(object sender, EventArgs e)
        {
            byte[] barray = new byte[1024];
            barray = Encoding.Unicode.GetBytes(Txtmsg.Text);
            SocClient.Send(barray);
        }
    }
}
